#! /usr/bin/env bash

export MODE="release"
source "$PWD"/dune.opts

git submodule update --init --recursive

if [[ ! -d $BUILDDIR/ch-preconditioner ]]; then
    if [[ -v DUNE_CONTROL_PATH ]]; then
	old_control="$DUNE_CONTROL_PATH"
	export DUNE_CONTROL_PATH=""
    fi

    pushd "$PWD"/dune-codegen
    ./patches/apply_patches.sh
    popd

    "$PWD"/dune-common/bin/dunecontrol --opts=dune.opts --module=dune-codegen all
    "$PWD"/dune-common/bin/dunecontrol --opts=dune.opts --only=dune-vectorclass all

    "$PWD"/dune-common/bin/dunecontrol --opts=dune.opts --only=ch-benchmarks cmake
    "$PWD"/dune-common/bin/dunecontrol --opts=dune.opts --only=ch-blockstructured cmake
    "$PWD"/dune-common/bin/dunecontrol --opts=dune.opts --only=ch-generation cmake
    "$PWD"/dune-common/bin/dunecontrol --opts=dune.opts --only=ch-preconditioner cmake

    if [[ -v old_control ]]; then
	export DUNE_CONTROL_PATH="$old_control"
    fi
fi

if [[ ! -d $PWD/build/release ]]; then
    ln -s "$BUILDDIR" "$PWD"/build/release
fi

export PATH="$PWD"/thesis-scripts:$PATH

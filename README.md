# thesis-supermodule

## Setup

This repository needs to be cloned with `--recursive`.

Executing the script `setup.sh` will configure and build the necessary Dune modules.
The thesis modules, prefixed with 'ch-', are not build, only configured.
All build directories are placed in `./build/release/`.
If the script is run with `source`, the `PATH` variable will be adjusted to contain the python scripts used for running the thesis' benchmarks.
Otherwise `PATH` needs to be adjusted by hand to contain `./thesis-scripts`.

Additionally, an installation of `likwid` ([repository](https://github.com/RRZE-HPC/likwid)) needs to be available for the benchmarks.

## Running the benchmarks

The benchmarks are grouped by chapter:
- chapter 3: `ch-blockstructured`

| section | dir                         | script                     |
|---------|-----------------------------|----------------------------|
| 3.4.1   | src/gather-scatter-overhead | gather-scatter-overhead.py |
| 3.4.2   | src/geometry-transformation | geometry_transformation.py |
| 3.4.3   | src/vectoritation           | vectorization.py           |
| 3.4.4   | src/loop-order              | operator_application.py    |

- chapter 4: `ch-preconditioner`

| section | dir | script            |
| ------- | --- | ---               |
| 4.3     | src | preconditioner.py |

- chapter 5: `ch-generation`

| section | dir                       | script                         |
|---------|---------------------------|--------------------------------|
| 5.4.1   | src/vectorization         | gen-vectorization.py           |
| 5.4.2   | src/licm                  | gen-geometry_transformation.py |
| 5.4.3   | src/opperator-application | gen-operator_application.py    |

- chapter 6: `ch-benchmarks`

| section | dir                | script           |
|---------|--------------------|------------------|
| 6.2.1   | apps/poisson       | poisson.py       |
| 6.2.2   | apps/p-laplacian   | p-laplacian.py   |
| 6.2.3   | apps/navier-stokes | navier-stokes.py |

- within each chapter directory, the target `all` builds all benchmarks for that chapter

Each benchmark is controlled by a script in `./thesis-scripts`.
All scripts use the following pattern:
- Run the benchmark with `script-name [run|r]` within the folder corresponding to the benchmark, i.e. `chapter-folder/src|apps/benchmark-folder`.
  The resulting data is located at `chapter-folder/results/benchmark-folder`.
- Process the resulting data with `script-name [process|p] -o outname` within the result folder.
  This will produce a gz compressed csv file named `outnamed`.
- With `script-name [visualize|v] -d outdir -i input` the `.csv.gz` file will be read and the corresponding plots will be created under `outdir`.
- More options are described by using `-h, --help`.

**Warning:** The running and processing only works for Intel Skylake architectures.
It may also work for other modern Intel CPUs, but it won't work for other architectures.
